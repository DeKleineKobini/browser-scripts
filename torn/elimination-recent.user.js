// ==UserScript==
// @name         TORN: Elimination - Recent Watch
// @version      1.0
// @description  Hide attacks not by or to allies.
// @author       DeKleineKobini
// @namespace    DeKleineKobini [2114440]
// @author       DeKleineKobini
// @match        https://www.torn.com/competition.php
// ==/UserScript==

(function() {
    'use strict';

    var allies = [];

    function hide(player) {
        player.style.display = 'none';
    }

    function show(player) {
        player.style.display = 'inline';
    }

    function isRecentPage() {
        return window.location.href.includes('recent');
    }

    function isListOfPlayers(node) {
        return node.classList !== undefined && node.classList.contains('recent-attacks-wrap');
    }

    function shouldHide(player) {
        return !(allies.includes(getAttackerTeam(player)) || allies.includes(getDefenderTeam(player)));
    }

    function getAttackerTeam(player){
        return player.firstElementChild.children[0].children[0].classList[1];
    }

    function getDefenderTeam(player){
        return player.firstElementChild.children[2].children[0].classList[1];
    }

    function applyFilter() {
        let playerList = document.querySelector('.competition-list');
        for (let player of playerList.children) {
            if(shouldHide(player)){
                hide(player);
            } else{
                show(player);
            }
        }
    }

    function watchForPlayerListUpdates() {
        let target = document.getElementById('competition-wrap');
        let observer = new MutationObserver(function(mutations) {
            if (!isRecentPage()) return;
            
            mutations.forEach(function(mutation) {
                let doApplyFilter = false;
                for (let i = 0; i < mutation.addedNodes.length; i++) {
                    if (isListOfPlayers(mutation.addedNodes.item(i))) {
                        console.log("doApplyFilter");
                        doApplyFilter = true;
                        break;
                    }
                }
                if (doApplyFilter) {
                    // updateIndices();
                    applyFilter();
                }
            });
        });
        // configuration of the observer:
        let config = { attributes: true, childList: true, characterData: true };
        // pass in the target node, as well as the observer options
        observer.observe(target, config);
    }
    watchForPlayerListUpdates();
})();