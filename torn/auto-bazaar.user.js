// ==UserScript==
// @name         TORN: Bazaar Auto Price
// @namespace    tos
// @version      0.5
// @description  description
// @author       tos
// @match        *.torn.com/bazaar.php*
// @require      https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js
// @require      https://greasyfork.org/scripts/39572-tornlib/code/tornlib.js?version=624765
// ==/UserScript==

requireAPI();

var apikey= localStorage.getItem("_apiKey");

var event = new Event('keyup')
var APIERROR = false

async function lmp(itemID) {
  if(APIERROR === true) return 'API key error'
  const res = await fetch(`https://api.torn.com/market/${itemID}?selections=bazaar,itemmarket&key=${apikey}`)
  const prices = await res.json()
  if (prices.error) {APIERROR = true;  return 'API key error'}
  let lowest_market_price = null
  for (const market in prices) {
    for (const lid in prices[market]) {
      if (lowest_market_price === null) lowest_market_price = prices[market][lid].cost
      else if (prices[market][lid].cost < lowest_market_price) lowest_market_price = prices[market][lid].cost
    }
  }
  return lowest_market_price - 1
}

const observer = new MutationObserver((mutations) => {
  for (const mutation of mutations) {
    for (const node of mutation.addedNodes) {
      if (node.classList && node.classList.contains('input-money-group')) {
        const li = node.closest('li.clearfix') || node.closest('li[id^=item]')
        const input = node.querySelector('.input-money[type=text]')
        if (li) {
          const itemID = li.querySelector('img').src.split('items/')[1].split('/medium')[0]
          input.addEventListener('focus', function(e) {
            if (this.id.includes('price-item')) this.value = ''
            if (this.value === '') {
              lmp(itemID).then((price) => {
                this.value = price
                this.dispatchEvent(event)
              })
            }
          })
        }
      }
    }
  }
})

const wrapper = document.querySelector('#bazaar-page-wrap')
observer.observe(wrapper, { subtree: true, childList: true })